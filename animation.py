def lerp(start, end, percent):
    if percent > 1:
        return end
    elif percent < 0:
        return start
    else:
        return (start * (1 - percent) + end * percent)


class Animation:
    def __init__(self, start_pos, end_pos, length):
        self.start_pos = start_pos
        self.end_pos = end_pos
        self.time = 0
        self.length = length
        self.finished = False
        self.playing = False

    def Start(self):
        self.playing = True
        self.time = 0

    def Reset(self):
        self.finished = False
        self.playing = False

    def Update(self):
        if self.playing:
            self.time += 1.0 / 60

        pos = (lerp(self.start_pos[0], self.end_pos[0],
                    self.time / self.length),
               lerp(self.start_pos[1], self.end_pos[1],
                    self.time / self.length))

        if self.time > self.length:
            self.finished = True
            self.playing = False
            self.time = 0

        return pos
