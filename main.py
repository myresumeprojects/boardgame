import pygame
from game import Game


def main():
    pygame.init()
    pygame.display.init()
    pygame.font.init()
    font = pygame.font.SysFont(pygame.font.get_default_font(), 30)
    ui_font = pygame.font.SysFont(pygame.font.get_default_font(), 40)
    screen = pygame.display.set_mode((1920, 1080))
    clock = pygame.time.Clock()
    game = Game(screen, font, ui_font)

    while game.running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                game.running = False

        game.Update(
            pygame.mouse.get_pressed(), pygame.mouse.get_pos(), pygame.key.get_pressed()
        )

        screen.fill("white")
        game.Draw()

        pygame.display.update()

        clock.tick(60)

    pygame.quit()


if __name__ == "__main__":
    main()
