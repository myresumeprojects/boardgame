import random


class Dice:
    def __init__(self, faces):
        self.faces = faces
        self.locked = False
        self.used = False


class DieFace:
    def __init__(self, face_type, amnt):
        self.face_type = face_type
        self.amnt = amnt
        self.surface = None


DIE_FACES = {"BLANK": 0, "DAMAGE": 1, "RANGE": 2, "SHIELD": 3, "MANA": 4, "HEAL": 5}

# DICE SETS
BaseDice = Dice(
    [
        DieFace(DIE_FACES["DAMAGE"], 3),
        DieFace(DIE_FACES["DAMAGE"], 2),
        DieFace(DIE_FACES["DAMAGE"], 1),
        DieFace(DIE_FACES["DAMAGE"], 1),
        DieFace(DIE_FACES["SHIELD"], 2),
        DieFace(DIE_FACES["SHIELD"], 1),
    ]
)
GoblinDice = Dice(
    [
        DieFace(DIE_FACES["DAMAGE"], 5),
        DieFace(DIE_FACES["DAMAGE"], 4),
        DieFace(DIE_FACES["BLANK"], 0),
        DieFace(DIE_FACES["BLANK"], 0),
        DieFace(DIE_FACES["DAMAGE"], 2),
        DieFace(DIE_FACES["DAMAGE"], 1),
    ]
)
GoblinSkirmisherDice = Dice(
    [
        DieFace(DIE_FACES["RANGE"], 4),
        DieFace(DIE_FACES["RANGE"], 3),
        DieFace(DIE_FACES["BLANK"], 0),
        DieFace(DIE_FACES["BLANK"], 0),
        DieFace(DIE_FACES["RANGE"], 2),
        DieFace(DIE_FACES["RANGE"], 1),
    ]
)

MitflitDice = Dice(
    [
        DieFace(DIE_FACES["DAMAGE"], 2),
        DieFace(DIE_FACES["DAMAGE"], 1),
        DieFace(DIE_FACES["BLANK"], 0),
        DieFace(DIE_FACES["BLANK"], 0),
        DieFace(DIE_FACES["DAMAGE"], 1),
        DieFace(DIE_FACES["DAMAGE"], 1),
    ]
)
MitflitSkirmisherDice = Dice(
    [
        DieFace(DIE_FACES["RANGE"], 2),
        DieFace(DIE_FACES["RANGE"], 1),
        DieFace(DIE_FACES["BLANK"], 0),
        DieFace(DIE_FACES["BLANK"], 0),
        DieFace(DIE_FACES["RANGE"], 1),
        DieFace(DIE_FACES["RANGE"], 1),
    ]
)

HobGoblinDice = Dice(
    [
        DieFace(DIE_FACES["DAMAGE"], 5),
        DieFace(DIE_FACES["DAMAGE"], 4),
        DieFace(DIE_FACES["SHIELD"], 2),
        DieFace(DIE_FACES["SHIELD"], 2),
        DieFace(DIE_FACES["DAMAGE"], 2),
        DieFace(DIE_FACES["DAMAGE"], 2),
    ]
)

DefenderDice = Dice(
    [
        DieFace(DIE_FACES["DAMAGE"], 1),
        DieFace(DIE_FACES["DAMAGE"], 1),
        DieFace(DIE_FACES["SHIELD"], 2),
        DieFace(DIE_FACES["SHIELD"], 2),
        DieFace(DIE_FACES["SHIELD"], 2),
        DieFace(DIE_FACES["SHIELD"], 1),
    ]
)
RangerDice = Dice(
    [
        DieFace(DIE_FACES["RANGE"], 3),
        DieFace(DIE_FACES["RANGE"], 2),
        DieFace(DIE_FACES["DAMAGE"], 1),
        DieFace(DIE_FACES["DAMAGE"], 1),
        DieFace(DIE_FACES["RANGE"], 2),
        DieFace(DIE_FACES["DAMAGE"], 1),
    ]
)
HealerDice = Dice(
    [
        DieFace(DIE_FACES["DAMAGE"], 1),
        DieFace(DIE_FACES["DAMAGE"], 1),
        DieFace(DIE_FACES["BLANK"], 0),
        DieFace(DIE_FACES["BLANK"], 0),
        DieFace(DIE_FACES["HEAL"], 1),
        DieFace(DIE_FACES["HEAL"], 2),
    ]
)


# HELPER FUNCTIONS FOR GAME ITSELF


def RollDice(die):
    return random.choice(die.faces)


def AssignPartyAndEnemyDice(game):
    for p in game.party:
        p.dice = AssignSurfaces(game, p.dice)
        p.rolled = p.dice.faces[0]
    for e in game.enemies:
        e.dice = AssignSurfaces(game, e.dice)
        e.rolled = e.dice.faces[0]


def AssignSurfaces(game, die):
    for face in die.faces:
        if DIE_FACES["BLANK"] == face.face_type:
            face.surface = game.art_manager.BLANKSurface
        if DIE_FACES["DAMAGE"] == face.face_type:
            face.surface = game.art_manager.DAMAGESurface
        if DIE_FACES["RANGE"] == face.face_type:
            face.surface = game.art_manager.RANGESurface
        if DIE_FACES["SHIELD"] == face.face_type:
            face.surface = game.art_manager.SHIELDSurface
        if DIE_FACES["HEAL"] == face.face_type:
            face.surface = game.art_manager.HEALSurface
        if DIE_FACES["MANA"] == face.face_type:
            face.surface = game.art_manager.MANASurface
        if DIE_FACES["DAMAGE"] == face.face_type:
            face.surface = game.art_manager.DAMAGESurface
        if DIE_FACES["DAMAGE"] == face.face_type:
            face.surface = game.art_manager.DAMAGESurface
    return die


def damageEntity(actor, target, amnt):
    if amnt >= target.shield_health:
        leftover_dmg = amnt - target.shield_health
        target.health -= leftover_dmg
        target.shield_health = 0
    else:
        target.shield_health -= amnt


def UseDice(game, dice, actor, target):
    if actor in game.party:
        actor_team = 0
    else:
        actor_team = 1
    if target in game.party:
        target_team = 0
    else:
        target_team = 1

    if DIE_FACES["BLANK"] == dice.face_type:
        return True

    if DIE_FACES["DAMAGE"] == dice.face_type:
        if actor_team != target_team:
            damageEntity(actor, target, dice.amnt)
            # TODO: ADD thorns mechanic, that hurts players in melee
            # but not ranged characters

            return True

    if DIE_FACES["RANGE"] == dice.face_type:
        if actor_team != target_team:
            damageEntity(actor, target, dice.amnt)
            return True

    if DIE_FACES["HEAL"] == dice.face_type:
        if actor_team == target_team:
            target.health += dice.amnt
            target.health = min(target.health, target.max_health)
            return True

    if DIE_FACES["SHIELD"] == dice.face_type:
        if actor_team == target_team:
            if target.health <= 0:
                return
            target.shield_health += dice.amnt
            return True

    return False
