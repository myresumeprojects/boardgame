import pygame

# import os
import art
import random
import entity
import dice
import map
import animation
import player

from music import MusicManager

# HELPER FUNCTIONS TO SEPERATE THE GAME FROM PYGAME


def drawCircle(dest, color, center, radius):
    pygame.draw.circle(dest, color, center, radius)


def drawEllipse(dest, color, rect, width=0):
    pygame.draw.ellipse(dest, color, rect, width=width)


def drawShadow(dest, color, rect, width=0, alpha=100):
    surface = pygame.Surface(rect.size).convert_alpha()
    surface.set_colorkey((0, 0, 0))
    pygame.draw.ellipse(surface, color, pygame.Rect((0, 0), rect.size), width)
    surface.set_alpha(alpha)
    dest.blit(surface, rect.topleft)


def drawRect(dest, color, rect, width=0):
    pygame.draw.rect(dest, color, rect, width=width)


def drawImage(dest, src, pos, scale=None):
    if src is None:
        return
    if scale is None:
        scale = src.get_size()
    src = pygame.transform.scale(src, scale)
    dest.blit(src, pos)


def collidePoint(rect, point):
    if rect is not None:
        return rect.collidepoint(point)
    else:
        return False


class Game:
    def __init__(self, screen, font, ui_font):
        self.screen = screen
        self.game_font = font
        self.ui_font = ui_font
        self.art_manager = art.ImageManager()
        self.mouse = None
        self.mouse_previous = None
        self.mouse_pos = None
        self.keys_previous = None
        self.running = True
        self.party = [
            entity.Entity(
                0, "Fighter", 5, 5, 0, dice.BaseDice, self.art_manager.FighterSurface
            ),
            entity.Entity(
                0,
                "Defender",
                5,
                5,
                0,
                dice.DefenderDice,
                self.art_manager.DefenderSurface,
            ),
            entity.Entity(
                0, "Ranger", 3, 3, 0, dice.RangerDice, self.art_manager.RangerSurface
            ),
            entity.Entity(
                0, "Healer", 3, 3, 0, dice.HealerDice, self.art_manager.HealerSurface
            ),
        ]
        self.dead_party = []
        self.enemies = []
        self.player_turn = True
        self.turn_start = True
        self.selected_idx = -1
        self.music_manager = MusicManager()

        self.animation = animation.Animation((0, 0), (0, 0), 1)

        self.mode = 2

        self.map = map.Map(self)
        self.player = player.Player()

        self.reroll_button = pygame.Rect(0, self.screen.get_height() - 200, 400, 200)
        self.end_turn_button = pygame.Rect(
            self.screen.get_width() - 400, self.screen.get_height() - 200, 400, 200
        )

        self.start_button = pygame.Rect(
            self.screen.get_width() / 2 - 200,
            self.screen.get_height() / 2 - 100,
            400,
            200,
        )

        self.music_manager.PlayMenuMusic()

        self.cloud_positions = []
        self.cloud_num = 20
        self.cloud_spawn_delay = 10000
        self.last_cloud_spawn = -self.cloud_spawn_delay

    # DRAW TEXT TO SCREEN / UI
    def DrawUIText(self, text, pos, color, background=None):
        image = self.ui_font.render(text, False, color, background)
        pos = (pos[0] - image.get_width() / 2, pos[1] - image.get_height() / 2)
        drawImage(self.screen, image, pos, image.get_size())

    def DrawText(self, text, pos, color, background=None):
        image = self.game_font.render(text, False, color, background)
        drawImage(self.screen, image, pos, image.get_size())

    def RemoveEnemies(self):
        for enemy in reversed(self.enemies):
            if enemy.health <= 0:
                self.enemies.remove(enemy)

    def EnemyTurn(self):
        self.RemoveEnemies()
        for enemy in reversed(self.enemies):
            if not enemy.dice.used:
                dice.UseDice(self, enemy.rolled, enemy, enemy.target)

    def StartCombatTurn(self):
        # Roll Dice for enemies and the party
        # allow dice to be used again

        for enemy in self.enemies:
            enemy.dice.used = False
            if not enemy.dice.locked:
                enemy.rolled = dice.RollDice(enemy.dice)
                if enemy.rolled.face_type == dice.DIE_FACES["SHIELD"]:
                    enemy.target = enemy
                else:
                    enemy.target = random.choice(self.party)
        for char in self.party:
            char.dice.used = False
            char.animation.length = 0.5
            if char.boundingRect:
                char.animation.start_pos = (char.boundingRect.x, char.boundingRect.y)
            if not char.dice.locked and char.health > 0:
                char.rolled = dice.RollDice(char.dice)

    def SelectTarget(self):
        target = None
        mouse_down = self.mouse[0] and not self.mouse_previous[0]

        for char in self.party:
            collidingBound = collidePoint(char.hitbox, self.mouse_pos)
            collidingCard = collidePoint(char.cardRect, self.mouse_pos)
            if (collidingBound or collidingCard) and mouse_down:
                target = char
        for enemy in self.enemies:
            collidingBound = collidePoint(enemy.hitbox, self.mouse_pos)
            collidingCard = collidePoint(enemy.cardRect, self.mouse_pos)
            if (collidingBound or collidingCard) and mouse_down:
                target = enemy
        return target

    def PlayerCombatTurn(self):
        # TURN SYSTEM
        # Things that happen at the start of the turn
        if self.turn_start:
            self.StartCombatTurn()
            self.turn_start = False
        else:
            # Things that happen during the turn
            # If a character is selected
            if self.selected_idx != -1:
                # Select a Target for the characters action
                target = self.SelectTarget()

                # If a target is selected Use the dice on the target
                if target is not None:
                    if dice.UseDice(
                        self,
                        self.party[self.selected_idx].rolled,
                        self.party[self.selected_idx],
                        target,
                    ):
                        self.party[self.selected_idx].animation.end_pos = (
                            target.hitbox.x,
                            target.hitbox.y,
                        )
                        self.party[self.selected_idx].animation.Start()
                        self.party[self.selected_idx].dice.used = True
                        self.selected_idx = -1
            else:
                # if a character is not selected
                mouse_down = self.mouse[0] and not self.mouse_previous[0]
                # if I click on a character select it
                for i, char in enumerate(self.party):
                    collidingBound = collidePoint(char.hitbox, self.mouse_pos)
                    collidingCard = collidePoint(char.cardRect, self.mouse_pos)
                    if collidingBound or collidingCard:
                        if mouse_down and not char.dice.used and char.health > 0:
                            self.selected_idx = i
                    # Set character selected
                    if self.selected_idx == i:
                        char.selected = True
                    else:
                        char.selected = False

            # Remove any enemies that have been killed by an action
            # There are more efficient ways of doing this
            # (specifically only after a dice is used)
            # But this can be changed later
            self.RemoveEnemies()

    def CombatMode(self):
        if len(self.enemies) == 0:
            self.turn_start = True
            self.mode = 1
            self.player.gold += 50
            return
        # End of Turn
        if collidePoint(self.end_turn_button, self.mouse_pos):
            if self.mouse[0] and not self.mouse_previous[0]:
                # Things that happen After The end of the turn
                # TODO: Better Monster AI
                self.EnemyTurn()

                # Remove character from the party if they are dead
                # Add to dead party so that we can get them back after
                # the fight (maybe)
                for char in reversed(self.party):
                    if char.health <= 0:
                        self.dead_party.append(char)
                        self.party.remove(char)

                # START NEXT TURN
                self.turn_start = True

        # Reroll the dice for all dice that are not locked
        if collidePoint(self.reroll_button, self.mouse_pos):
            if self.mouse[0] and not self.mouse_previous[0]:
                for char in self.party:
                    if not char.dice.locked:
                        char.rolled = dice.RollDice(char.dice)

        if self.mouse[0] == self.mouse_previous:
            self.turn_start = True

        if self.player_turn:
            self.PlayerCombatTurn()

    def StartMapTurn(self):
        move = 1  # random.randint(1, 6) + random.randint(1, 6)
        self.map.index += move
        self.animation.start_pos = (550, 400)
        self.animation.end_pos = (900, 400)
        self.animation.Reset()
        for char in self.party:
            char.animation.Reset()

    def MapMode(self):
        if self.turn_start:
            self.StartMapTurn()
            self.turn_start = False
        else:
            if collidePoint(self.screen.get_rect(), self.mouse_pos):
                if self.mouse[0] and not self.mouse_previous[0]:
                    self.animation.Start()

            if self.animation.finished:
                # if animation is finished then go to the next screen
                if self.map.tiles[self.map.index].tile_type != 3:
                    # If the tile is an encounter tile, then start the encounter
                    self.mode = 0
                    self.turn_start = True
                    print(self.map.tiles[self.map.index].encounter)
                    self.enemies = self.map.tiles[self.map.index].encounter.copy()
                    dice.AssignPartyAndEnemyDice(self)
                elif self.map.tiles[self.map.index].tile_type == 3:
                    # First Aid kinda tile. Proof of concept, probably change later
                    for character in self.party:
                        character.health += random.randint(1, 3)
                        character.health = min(character.health, character.max_health)

                    self.turn_start = True
                else:
                    # TODO: Make additional tiles that are not encounter tile

                    pass

    def MainMenuMode(self):
        if collidePoint(self.start_button, self.mouse_pos):
            if self.mouse[0] and not self.mouse_previous[0]:
                self.mode = 0
                dice.AssignPartyAndEnemyDice(self)
                self.cloud_positions = []

        if (
            len(self.cloud_positions) < self.cloud_num
            and pygame.time.get_ticks() > self.last_cloud_spawn + self.cloud_spawn_delay
        ):
            self.cloud_positions.append((-64, random.randint(50, 300)))
            self.last_cloud_spawn = pygame.time.get_ticks()

        for i, cloud_pos in enumerate(self.cloud_positions):
            self.cloud_positions[i] = (
                cloud_pos[0] + (10) / 30,
                cloud_pos[1],
            )
            if cloud_pos[0] > self.screen.get_width() + 64:
                self.cloud_positions[i] = (-64, random.randint(50, 300))

    def Update(self, mouse, mouse_pos, keys):
        self.mouse = mouse
        self.mouse_pos = mouse_pos

        if self.mouse_previous is None:
            self.mouse_previous = mouse

        if self.keys_previous is None:
            self.keys_previous = keys

        if self.mode == 0:
            self.CombatMode()
        if self.mode == 1:
            self.MapMode()
        if self.mode == 2:
            self.MainMenuMode()

        # UPDATE KEYS PREVIOUS
        self.keys_previous = keys
        # UPDATE MOUSE PREVIOUS
        self.mouse_previous = mouse
        # UPATE MUSIC
        self.music_manager.Update()

    def DrawCombat(self):
        # DRAW BACKGROUND
        drawImage(self.screen, self.art_manager.BackgroundSurface, (0, 0), (1920, 1080))

        # DRAW ENEMIES AND ALLIES
        # UI COLORS
        text_color = (200, 200, 200)

        # DICE COLORS
        available_color = "green"
        used_color = "grey"
        locked_color = "red"

        # POSITIONAL ARGUMENTS
        starting_x = 700
        starting_y = self.screen.get_height() - 650
        enemy_buffer = 400
        x = starting_x
        dy = 500 / (len(self.party) + 1)
        y = starting_y + dy
        char_card_y = 10

        for char in self.party:
            char.boundingRect = pygame.Rect(x, y + 25, 100, 100)
            char.animation.start_pos = char.boundingRect.topleft
            char.hitbox = pygame.Rect(x, y, 100, 150)
            # Draw Shadow
            if char.selected:
                drawShadow(self.screen, "yellow", pygame.Rect(x, y + 100, 100, 50))
            else:
                drawShadow(self.screen, (1, 1, 1), pygame.Rect(x, y + 100, 100, 50))
            # Draw Character
            drawImage(self.screen, char.surface, char.animation.Update(), (100, 100))
            # Set Dice Color
            dice_color = available_color
            if char.dice.used:
                dice_color = used_color
            elif char.dice.locked:
                dice_color = locked_color

            # Draw Character UI
            if char.selected:
                selected_offset = 100
            else:
                selected_offset = 0

            char_text = f"{char.name} : {char.health} / {char.max_health}"
            character_info = self.game_font.render(char_text, False, "grey", "black")
            char.cardRect = pygame.Rect(
                selected_offset, char_card_y, 250, character_info.get_height() + 100
            )

            drawRect(self.screen, "black", char.cardRect)
            drawRect(self.screen, (200, 100, 100), char.cardRect, width=5)

            self.DrawText(
                f"{char.name} : {char.health} / {char.max_health}",
                (
                    char.cardRect.x
                    + char.cardRect.width / 2
                    - character_info.get_width() / 2,
                    char.cardRect.y + 15,
                ),
                text_color,
            )

            drawRect(
                self.screen,
                dice_color,
                pygame.Rect(
                    char.cardRect.x + 30,
                    char.cardRect.y
                    + character_info.get_height()
                    + char.cardRect.height / 2
                    - 30,
                    60,
                    60,
                ),
            )
            drawImage(
                self.screen,
                char.rolled.surface,
                (
                    char.cardRect.x + 40,
                    char.cardRect.y
                    + character_info.get_height()
                    + char.cardRect.height / 2
                    - 20,
                ),
                (40, 40),
            )
            self.DrawText(
                f"{char.rolled.amnt}",
                (
                    char.cardRect.x + 100,
                    char.cardRect.y
                    + character_info.get_height()
                    + char.cardRect.height / 2
                    + 15,
                ),
                text_color,
            )
            drawImage(
                self.screen,
                char.surface,
                (
                    char.cardRect.x + 150,
                    char.cardRect.y + char.cardRect.height / 2 - 20,
                ),
                (60, 60),
            )

            if char.shield_health > 0:
                drawImage(
                    self.screen,
                    self.art_manager.SHIELDSurface,
                    (char.cardRect.x + char.cardRect.width + 20, char.cardRect.y),
                    (80, 80),
                )
                self.DrawText(
                    f"{char.shield_health}",
                    (char.cardRect.x + char.cardRect.width + 55, char.cardRect.y + 30),
                    "black",
                )

            # Draw Character Info
            x -= 50
            y += dy
            char_card_y += 200

        enemy_card_y = 10
        x = starting_x + enemy_buffer
        dy = 500 / (len(self.enemies) + 1)
        y = starting_y + dy
        for enemy in self.enemies:
            # Set Enemy Bounding Rect
            enemy.boundingRect = pygame.Rect(x, y, 100, 100)
            enemy.hitbox = pygame.Rect(x, y, 100, 150)
            # Get Enemy Info
            enemy_info = self.game_font.render(
                f"{enemy.name} : {enemy.health} / {enemy.max_health}",
                False,
                "black",
                None,
            )
            # Set The Enemy Card
            enemy.cardRect = pygame.Rect(
                self.screen.get_width() - 250,
                enemy_card_y,
                250,
                enemy_info.get_height() + 150,
            )
            # Set The Enemy Dice Rect
            enemy.diceRect = pygame.Rect(
                enemy.cardRect.x + enemy.cardRect.width - 140,
                enemy.cardRect.y
                + enemy_info.get_height()
                + enemy.cardRect.height / 2
                - 50,
                100,
                100,
            )
            # Start Drawing The Enemy Card
            drawRect(self.screen, "black", enemy.cardRect)
            drawRect(self.screen, (240, 100, 100), enemy.cardRect, width=5)
            drawShadow(self.screen, (1, 1, 1), pygame.Rect(x, y + 100, 100, 50))
            drawImage(self.screen, enemy.surface, (x, y + 25), (100, 100))
            drawImage(
                self.screen,
                enemy.surface,
                (
                    enemy.cardRect.x + 20,
                    enemy.cardRect.y + enemy.cardRect.height / 2 - 25,
                ),
                (50, 50),
            )
            drawRect(self.screen, "red", enemy.diceRect)
            enemy_text = f"{enemy.name} : {enemy.health} / {enemy.max_health}"
            self.DrawText(
                enemy_text,
                (
                    enemy.cardRect.x
                    + enemy.cardRect.width / 2
                    - enemy_info.get_width() / 2,
                    enemy.cardRect.y + 15,
                ),
                text_color,
            )
            drawImage(
                self.screen,
                enemy.rolled.surface,
                (enemy.diceRect.x + 10, enemy.diceRect.y + 10),
                (80, 80),
            )
            self.DrawText(
                f"{enemy.rolled.amnt}",
                ((enemy.diceRect.x + 80, enemy.diceRect.y + 80)),
                text_color,
            )
            if enemy.target is not None:
                pygame.draw.line(
                    self.screen,
                    "red",
                    enemy.boundingRect.midleft,
                    enemy.target.boundingRect.midright,
                    width=5,
                )

            if enemy.shield_health > 0:
                drawImage(
                    self.screen,
                    self.art_manager.SHIELDSurface,
                    (enemy.cardRect.x - 100, enemy.cardRect.y),
                    (80, 80),
                )
                self.DrawText(
                    f"{enemy.shield_health}",
                    (enemy.cardRect.x - 100 + 35, enemy.cardRect.y + 30),
                    "black",
                )

            x += 50
            y += dy
            enemy_card_y += 240

        # DRAW UI ELEMENTS

        drawRect(self.screen, "black", self.end_turn_button)
        drawRect(self.screen, "grey", self.end_turn_button, 5)

        self.DrawUIText(
            "End Turn",
            (
                self.screen.get_width() - self.end_turn_button.width / 2,
                self.screen.get_height() - self.end_turn_button.height / 2,
            ),
            text_color,
        )

        drawRect(self.screen, "black", self.reroll_button)
        drawRect(self.screen, "grey", self.reroll_button, 5)

        self.DrawUIText(
            "Re-roll",
            (
                self.reroll_button.width / 2,
                self.screen.get_height() - self.reroll_button.height / 2,
            ),
            text_color,
        )

    def DrawTile(self, text, description, text_color, rect, top_color, base_color):
        drawRect(self.screen, base_color, rect)
        drawRect(self.screen, top_color, pygame.Rect(rect.x, rect.y, rect.width, 100))
        drawRect(
            self.screen,
            (0, 0, 0),
            pygame.Rect(rect.x, rect.y, rect.width, 100),
            width=20,
        )
        drawRect(self.screen, (0, 0, 0), rect, width=20)
        self.DrawUIText(text, (rect.x + rect.width / 2, rect.y + 140), text_color)
        self.DrawUIText(
            description, (rect.x + rect.width / 2, rect.y + 240), text_color
        )

    def DrawMap(self):
        text_color = (200, 200, 200)
        self.screen.fill((0, 150, 250))
        for i, tile in enumerate(self.map.tiles):
            self.DrawTile(
                tile.name,
                tile.description,
                text_color,
                pygame.Rect((i - self.map.index + 2) * 400, 200, 400, 400),
                tile.top_color,
                tile.base_color,
            )
        self.DrawUIText(f"{self.map.index}", (50, 50), text_color)
        self.DrawUIText(
            f"{self.player.gold}", (self.screen.get_width() - 200, 50), text_color
        )
        drawImage(
            self.screen, self.party[0].surface, self.animation.Update(), (100, 100)
        )

    def DrawMainMenu(self):
        text_color = (200, 200, 200)
        self.screen.fill((0, 0, 0))
        # DRAW MAIN MENU BACKGROUND
        drawImage(
            self.screen,
            self.art_manager.mainMenuBackgroundSurface,
            (0, 0),
            (self.screen.get_width(), self.screen.get_height()),
        )

        for cloud_pos in self.cloud_positions:
            drawImage(
                self.screen,
                self.art_manager.cloudSurfaces[0],
                cloud_pos,
                (
                    self.art_manager.cloudSurfaces[0].get_width(),
                    self.art_manager.cloudSurfaces[0].get_height(),
                ),
            )

        # DRAW UI
        if collidePoint(self.start_button, self.mouse_pos):
            drawRect(self.screen, (100, 100, 100), self.start_button, width=20)
        else:
            drawRect(self.screen, (100, 100, 100), self.start_button, width=10)

        drawImage(
            self.screen,
            self.art_manager.titleSurface,
            (
                (self.screen.get_width() - self.art_manager.titleSurface.get_width())
                / 2,
                self.screen.get_height() / 4,
            ),
        )

        self.DrawUIText(
            "Start Game",
            (self.start_button.centerx, self.start_button.centery),
            text_color,
        )

    def Draw(self):
        if self.mode == 0:
            self.DrawCombat()
        elif self.mode == 1:
            self.DrawMap()
        elif self.mode == 2:
            self.DrawMainMenu()
