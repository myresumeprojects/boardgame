import random
import entity


class Tile:
    type_colors = [(50, 200, 50), (200, 100, 40), (50, 50, 200), (200, 50, 50)]
    base_colors = [(100, 50, 0)]

    def __init__(self, name, description, tile_type, biome_type, encounter):
        self.name = name
        self.description = description
        self.tile_type = tile_type
        self.biome_type = biome_type
        self.top_color = self.type_colors[self.tile_type]  # Determined by tile type
        self.base_color = self.base_colors[self.biome_type]  # Determined by biome ???

        self.encounter = (
            encounter  # eventually determined by both above (tile type and biome)
        )


class Map:
    tile_first_name = ["Hidden", "Goblin", "Bandit", "King's", "Witch's", ""]
    tile_last_name = [
        "Bog",
        "Den",
        "Gallows",
        "Path",
        "Road",
        "Field",
        "Forest",
        "Glenn",
        "Caves",
        "Abyss",
    ]

    def __init__(self, game):
        self.tiles = []
        self.index = 0
        self.bestiary = entity.Bestiary(game.art_manager)
        self.GenerateMap()

    def GenerateMap(self):
        for i in range(50):
            tile_type = random.randint(0, len(Tile.type_colors) - 1)
            if tile_type == 0:
                tile_encounter = self.bestiary.CreateEncounter(1)
            elif tile_type == 1:
                tile_encounter = self.bestiary.CreateEncounter(2)
            else:
                tile_encounter = self.bestiary.CreateEncounter(3)

            if tile_type == 3:
                description = "HEAL 1d3 HP"
            else:
                description = "Encounter"

            self.tiles.append(
                Tile(
                    random.choice(self.tile_first_name)
                    + " "
                    + random.choice(self.tile_last_name),
                    description,
                    tile_type,
                    0,
                    tile_encounter,
                )
            )
            # print(self.tiles[-1].encounter)
