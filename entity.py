import animation
import dice
import random


class Bestiary:
    def __init__(self, art_manager):
        self.woundedGoblin = Entity(
            1, "Goblin (Injured)", 2, 5, 0, dice.GoblinDice, art_manager.GoblinSurface
        )
        self.goblin = Entity(
            2, "Goblin", 5, 5, 0, dice.GoblinDice, art_manager.GoblinSurface
        )
        self.armoredGoblin = Entity(
            3,
            "Armored Goblin",
            5,
            5,
            5,
            dice.GoblinDice,
            art_manager.ArmoredGoblinSurface,
        )
        self.commandoGoblin = Entity(
            3,
            "Goblin Commando",
            7,
            7,
            3,
            dice.GoblinDice,
            art_manager.CommandoGoblinSurface,
        )
        self.skirmisherGoblin = Entity(
            2,
            "Goblin Skirmisher",
            5,
            5,
            0,
            dice.GoblinSkirmisherDice,
            art_manager.GoblinSkirmisherSurface,
        )

        self.mitflit = Entity(
            1, "Mitflit", 3, 3, 0, dice.MitflitDice, art_manager.MitflitSurface
        )
        self.skirmisherMitflit = Entity(
            1,
            "Mitflit Skirmisher",
            3,
            3,
            0,
            dice.MitflitSkirmisherDice,
            art_manager.MitflitSkirmisherSurface,
        )

        self.hobgoblin = Entity(
            4, "Hob-Goblin", 7, 7, 1, dice.HobGoblinDice, art_manager.HobGoblinSurface
        )

        self.Monsters = [
            [self.woundedGoblin, self.mitflit, self.skirmisherMitflit],
            [self.goblin, self.skirmisherGoblin],
            [self.commandoGoblin, self.armoredGoblin],
            [self.hobgoblin],
        ]

    def CreateEncounter(self, budget):
        # TODO: CREATE SMARTER ENCOUNTER BUILDER
        # Currentlly a budget based encounter BUILDER
        # Want to be able to specify what enemies we want for semi scripted encounters
        enemies = []
        while budget > 0:
            x = random.randint(0, min(len(self.Monsters) - 1, budget))
            y = random.randint(0, len(self.Monsters[x]) - 1)
            enemies.append(self.Monsters[x][y].copy())
            budget -= x + 1

        return enemies.copy()


class Entity:
    def __init__(self, cost, name, health, max_health, shield_health, dice, surface):
        self.cost = cost  # THE AMOUNT THE ENTITY COSTS IN THE ENCOUNTER BUDGET (used to be used, not anymore, might be used again)
        self.name = name  # NAME OF THE ENTITY
        self.health = health  # CURRENT ENTITY HEALTH
        self.max_health = max_health  # MAX ENTITY health
        self.shield_health = shield_health  # CURRENT ENTITY SHIELD HEALTH
        self.dice = dice  # DICE ENTITY ROLLS
        self.rolled = dice.faces[0]  # FACE DISPLAYED NEXT TO ENTITY
        self.surface = surface  # ICON FOR THE ENTITY
        self.selected = False  # ENTITY IS SELECTED DURING TURN
        self.boundingRect = None  # RECT FOR KNOWING POSITION
        self.hitbox = None  # RECT FOR SELECTING ENTITY
        self.cardRect = None  # RECT FOR SELECTING ENTITY AND FOR DISPLAYING
        # UI INFO
        self.diceRect = None  # RECT FOR SELECTING DICE
        self.target = None  # ONLY USED FOR ENEMY RN
        self.animation = animation.Animation((0, 0), (100, 0), 1)

    def copy(self):
        return Entity(
            self.cost,
            self.name,
            self.health,
            self.max_health,
            self.shield_health,
            self.dice,
            self.surface,
        )
