import pygame
import os


def extract_animation(sheet, start, size, num_frames):
    frames = []
    for i in range(num_frames):
        frames.append(
            sheet.subsurface(pygame.Rect((start[0] + i * size[0], start[1]), size))
        )
    return frames.copy()


class ImageManager:
    def __init__(self):
        # BackgroundSurface
        self.BackgroundSurface = pygame.image.load(
            os.path.join(".", "VFX", "battleground_highlands_0001.png")
        ).convert()
        self.mainMenuBackgroundSurface = pygame.image.load(
            os.path.join(".", "VFX", "main_menu_background.png")
        ).convert()
        self.titleSurface = pygame.image.load(
            os.path.join(".", "VFX", "title_image.png")
        ).convert_alpha()
        self.cloudSheet = pygame.image.load(
            os.path.join(".", "VFX", "Cloud-Sheet.png")
        ).convert_alpha()
        self.cloudSurfaces = extract_animation(self.cloudSheet, (0, 0), (64, 64), 5)
        self.TextureAtlas = pygame.image.load(
            os.path.join(".", "VFX", "urizen_onebit_tileset__v1d0.png")
        ).convert_alpha()
        self.TextureAtlas.set_colorkey((255, 0, 255))

        # ENTITY ASSETS
        self.FighterSurface = self.TextureAtlas.subsurface(
            pygame.Rect(339 + 13 * 4, 1, 12, 12)
        ).convert_alpha()
        self.FighterSurface.set_colorkey((255, 0, 255))
        self.DefenderSurface = self.TextureAtlas.subsurface(
            pygame.Rect(339 + 13 * 1, 1, 12, 12)
        ).convert_alpha()
        self.DefenderSurface.set_colorkey((255, 0, 255))
        self.RangerSurface = self.TextureAtlas.subsurface(
            pygame.Rect(339, 1, 12, 12)
        ).convert_alpha()
        self.RangerSurface.set_colorkey((255, 0, 255))
        self.HealerSurface = self.TextureAtlas.subsurface(
            pygame.Rect(365, 1, 12, 12)
        ).convert_alpha()
        self.HealerSurface.set_colorkey((255, 0, 255))

        self.GoblinSurface = self.TextureAtlas.subsurface(
            pygame.Rect(339, 13 * 2 + 1, 12, 12)
        ).convert_alpha()
        self.GoblinSurface.set_colorkey((255, 0, 255))
        self.ArmoredGoblinSurface = self.TextureAtlas.subsurface(
            pygame.Rect(443, 13 * 2 + 1, 12, 12)
        ).convert_alpha()
        self.ArmoredGoblinSurface.set_colorkey((255, 0, 255))
        self.CommandoGoblinSurface = self.TextureAtlas.subsurface(
            pygame.Rect(378, 13 * 2 + 1, 12, 12)
        ).convert_alpha()
        self.CommandoGoblinSurface.set_colorkey((255, 0, 255))
        self.GoblinSkirmisherSurface = self.TextureAtlas.subsurface(
            pygame.Rect(365, 13 * 2 + 1, 12, 12)
        ).convert_alpha()
        self.GoblinSkirmisherSurface.set_colorkey((255, 0, 255))
        self.MitflitSurface = self.TextureAtlas.subsurface(
            pygame.Rect(339, 13 * 8 + 1, 12, 12)
        ).convert_alpha()
        self.MitflitSurface.set_colorkey((255, 0, 255))
        self.MitflitSkirmisherSurface = self.TextureAtlas.subsurface(
            pygame.Rect(378, 13 * 8 + 1, 12, 12)
        ).convert_alpha()
        self.MitflitSkirmisherSurface.set_colorkey((255, 0, 255))
        self.HobGoblinSurface = self.TextureAtlas.subsurface(
            pygame.Rect(339, 13 * 3 + 1, 12, 12)
        ).convert_alpha()
        self.HobGoblinSurface.set_colorkey((255, 0, 255))

        # DICE ASSETS
        self.BLANKSurface = self.TextureAtlas.subsurface(
            pygame.Rect(1015, 495, 12, 12)
        ).convert_alpha()
        self.BLANKSurface.set_colorkey((255, 0, 255))
        self.DAMAGESurface = self.TextureAtlas.subsurface(
            pygame.Rect(339, 339, 12, 12)
        ).convert_alpha()
        self.DAMAGESurface.set_colorkey((255, 0, 255))
        self.RANGESurface = self.TextureAtlas.subsurface(
            pygame.Rect(339, 339 + 13 * 3, 12, 12)
        ).convert_alpha()
        self.RANGESurface.set_colorkey((255, 0, 255))
        self.SHIELDSurface = self.TextureAtlas.subsurface(
            pygame.Rect(339, 339 + 13 * 5, 12, 12)
        ).convert_alpha()
        self.SHIELDSurface.set_colorkey((255, 0, 255))
        self.HEALSurface = self.TextureAtlas.subsurface(
            pygame.Rect(1028, 482, 12, 12)
        ).convert_alpha()
        self.HEALSurface.set_colorkey((255, 0, 255))
        self.MANASurface = self.TextureAtlas.subsurface(
            pygame.Rect(1080, 482, 12, 12)
        ).convert_alpha()
        self.MANASurface.set_colorkey((255, 0, 255))
