# Patrol
## Game Description
This game intends to be a short project inspired by the game Slice and Dice. The main difference between the two is going to be an over-world map,
in which it will kinda play like monopoly, where you roll dice to see how far you will travel, and then interact with the location that you land on.
Making it around the board and back to what would be "Go" in monopoly signifies completing the patrol and winning the run.
When traveling around the "board", the player should be able to construct buildings that will help them in future runs, such as an inn where the characters might be able to get a temporary boost to their stats or dice that lasts for an amount of combats.

## TODO In No Particular Order
- [ ] Impliment Options Menu
- [ ] Create Encounter System
    - [ ] Random Encounter Generation
        - [ ] Level Based Encounter Generation
        - [ ] Area Based Encounter Generation (maybe) (chose one)
    - [ ] Boss Encounters 
- [ ] Impliment Items
- [ ] Create More Items
- [ ] Create More Enemies
    - [X] Create HobGoblin
    - [X] Create Armored Goblin
    - [X] Create Commando Goblin
    - [X] Create Goblin Skirmisher
    - [X] Create Mitflit Skirmisher
- [ ] Create More Characters
- [ ] Create Character Level Up System
- [ ] Create Status Effects
- [ ] Impliment Mana
- [ ] Impliment Spell System
- [ ] Create More Spells
- [ ] Impliment Healing
- [ ] Impliment Revival
- [ ] Impliment Sound Effects (SFX)
- [ ] Impliment animations
    - [ ] Attack animations
    - [ ] Sheild animations
    - [ ] Hit animations
    - [ ] Heal animations
    - [ ] Mana animations
    - [ ] Spell animations
- [ ] Impliment Particle Effects (kinda like animations but not)
- [ ] Create World Map / Board View
    - [ ] Create Locations
    - [ ] Create Biomes, Getting harder the closer you are to finishing the patrol
- [ ] Impliment UI Interactions
    - [ ] Hover Response for Buttons
    - [ ] Click Response for Buttons
    - [ ] Sliders for volume and SFX

## Done 
- [X] Impliment Main Menu
- [x] Create Background
- [x] Impliment Dice Rolling Functionality
- [x] Impliment Music
- [x] Impliment Rerolling
- [x] Impliment End of Turn
- [x] Impliment Buttons
- [x] Add Enemies
- [x] Add Enemies AI 
- [x] Added Characters
- [x] Impliment Sheilding
- [x] Impliment Damage

