import os
import random
import pygame

challenge_fate = os.path.join(".", "SFX", "BGM", "Action_-_Challenge_Fate.ogg")
adventure_awaits = os.path.join(".", "SFX", "BGM", "Menu_-_Adventures_Await.ogg")
castle_master = os.path.join(".", "SFX", "BGM", "Menu_-_Castle_Master.ogg")
dreaming_darkly = os.path.join(".", "SFX", "BGM", "Menu_-_Dreaming_Darkly.ogg")
epic_entrance = os.path.join(".", "SFX", "BGM", "Menu_-_Epic_Entrance.ogg")
noble_kingdom = os.path.join(".", "SFX", "BGM", "Menu_-_Noble_Kingdom.ogg")

menu_music_tracks = [adventure_awaits, castle_master, dreaming_darkly, epic_entrance, noble_kingdom]
action_music_tracks = [challenge_fate]

class MusicManager:
    def __init__(self):
        self.current_playlist = None
        self.menu_music_tracks = menu_music_tracks
        self.action_music_tracks = action_music_tracks
        self.volume = 0.5

        pygame.mixer.music.set_volume(self.volume)

    def PlayMusic(self):
        pygame.mixer.music.load(random.choice(self.current_playlist))
        pygame.mixer.music.play(0, 0, 0)

    def PlayMenuMusic(self):
        self.current_playlist = self.menu_music_tracks
        self.PlayMusic()
           
    def PlayActionMusic(self):
        self.current_playlist = self.action_music_tracks
        self.PlayMusic()

    def Update(self):
        if not pygame.mixer.music.get_busy():
            self.PlayMusic()

    def SetVolume(self, vol):
        self.volume = vol
        pygame.mixer.music.set_volume(self.volume)
    
    def GetVolume(self):
        return self.volume
